import React from "react"

export default function Home() {
  return (
    <div>
      <h1>Full Stack Citation Courses</h1>
      <table>
        <thead>
            <tr>
                <th>Subject</th>
                <th>Course</th>
                <th>Title</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  )
}
